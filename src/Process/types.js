/* istanbul ignore file */
export function isFunction(fn) {
  return fn && typeof fn === 'function';
}

export function isString(s) {
  return s && typeof s === 'string';
}

export function isPromise(p) {
  return p instanceof Promise;
}

export function isIterator(iter) {
  return iter && isFunction(iter.next);
}

const GeneratorFunction = (function*() {}).constructor;
const AsyncGeneratorFunction = (async function*() {}).constructor;

export function isGeneratorFunction(f) {
  return f instanceof GeneratorFunction || f instanceof AsyncGeneratorFunction;
}
