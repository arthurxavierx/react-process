import React, { Component, Fragment as ReactFragment } from 'react';
import { isFunction, isString } from './types.js';
import {
  Process,
  Yield,
  Await,
  Forever as ForeverProcess,
  Race as RaceProcess,
  All as AllProcess
} from './index.js';

/**
 * Create a React component from a process.
 *
 * The JSX outputs produced by the process are composed as the component's
 * rendering function.
 *
 * After the process finishes, the generated component's `onFinish` prop is
 * called.
 */
export default function componentFromProcess(process) {
  return class ProcessComponent extends Component {
    constructor(props) {
      super(props);
      this.state = { view: null };
      this._process = Process.of(process);
    }

    componentDidMount() {
      this._process
        .run(view => this.setState({ view }))
        .then(a => {
          if (isFunction(a))
            return this.props.onFinish(a)
        });
    }

    componentWillUnmount() {
      this._process.cancel();
    }

    render() {
      return this.state.view;
    }
  };
}

/**
 * Custom JSX syntax transformer. This should be used with
 * babel-plugin-transform-react-jsx, as a @jsx pragma.
 *
 * Lowercase HTML elements are transformed into processes that produce such HTML
 * element as a single output and then finish with an `undefined` return value.
 *
 * Lowercase HTML elements with children are transformed into wrappers that race
 * all of the child processes.
 */
export function jsx(c, props, ...arr) {
  const children = arr.flat(Infinity).map(el => jsx(el));

  if (isFunction(c)) {
    return jsx(c({ children, ...props }));
  }
  else if (!!props && (isString(c) || c instanceof Component)) {
    return children.length > 0
      ? new RaceProcess(children, children_ => React.createElement(c, props, children_))
      : new Yield(React.createElement(c, props));
  }
  else {
    return Process.of(c, undefined, a => new Yield(a));
  }
}

/**
 * TODO: write documentation
 */
export function render(eff) {
  return new Await(eff);
}

/**
 * Runs all child processes in parallel but never finishes.
 */
export function Forever({ children }) {
  return new ForeverProcess(children, c => React.createElement(ReactFragment, {}, ...c));
}

/**
 * Performs the alternative composition of all of the child processes.
 * The first child process to end wins the race and causes the parent process to
 * end with its return value.
 */
export function Race({ children }) {
  return new RaceProcess(children, c => React.createElement(ReactFragment, {}, ...c));
}

export const Fragment = Race;

/**
 * Performs the parallel composition of all of the child processes.
 * The parent process only ends when all of the child processes have ended.
 * Its return value is the list of the return values of all child processes.
 */
export function All({ children }) {
  return new AllProcess (children, c => React.createElement(ReactFragment, {}, ...c));
}
