import { Forever, Race, All } from './index.js';

// delay :: Int -> Process o ()
export function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// loop :: ((_ -> IO ()) -> Process o a) -> Process o a
export function* loop(f) {
  let done = false;
  do {
    yield f(_ => done = true);
  } while (!done);
}

// all :: Array (Process o a) -> (Array o -> o') -> Process o' a
export function all(ps, combine) {
  return new All(ps, combine);
}

// race :: Array (Process o a) -> (Array o -> o') -> Process o' a
export function race(ps, combine) {
  return new Race(ps, combine);
}

// forever :: Process o a -> Process o b
export function forever(p) {
  return new Forever([p]);
}

// foreverMany :: Array (Process o a) -> (Array o -> o') -> Process o' b
export function foreverMany(ps, combine) {
  return new Forever(ps, combine);
}
