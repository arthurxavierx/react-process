import CancellationToken from './CancellationToken';
import {
  isFunction,
  isGeneratorFunction,
  isIterator,
  isPromise
} from './types';

const identity = x => x;

export class Process {
  constructor(cancellationToken = undefined) {
    this._cancellationToken = cancellationToken instanceof CancellationToken
      ? cancellationToken
      : new CancellationToken();
  }

  cancel() {
    this._cancellationToken.cancel();
  }

  get isCancelled() {
    return this._cancellationToken.isCancelled;
  }

  static of(a, cancellationToken = undefined, def = a => new FromPromise(a)) {
    if (a instanceof Process) {
      if (cancellationToken instanceof CancellationToken)
        a._cancellationToken = cancellationToken;
      return a;
    }
    else if (isPromise(a)) {
      return new FromPromise(a);
    }
    else if (isGeneratorFunction(a)) {
      return new FromIterator(a(), cancellationToken);
    }
    else if (isIterator(a)) {
      return new FromIterator(a, cancellationToken);
    }
    else {
      return def(a);
    }
  }
}

export class Yield extends Process {
  constructor(output, result = null) {
    super();
    this._output = output;
    this._result = result;
  }

  map(fn) {
    const p = new Yield(this._output, fn(this._result));
    p._cancellationToken = this._cancellationToken;
    return p;
  }

  run(handle) {
    if (this.isCancelled) return;
    handle(this._output);
    return Promise.resolve(this._result);
  }
}

export class Await extends Process {
  constructor(executor) {
    super();
    this.executor = executor;
  }

  map(fn) {
    const p = new Await(done => this.executor(a => done(fn(a))));
    p._cancellationToken = this._cancellationToken;
    return p;
  }

  run(handle) {
    return new Promise(resolve => {
      if (this.isCancelled) return;

      const p = Process.of(this.executor(a => {
        if (!this.isCancelled)
          resolve(a);
      }));
      p.run(o => {
        if (!this.isCancelled)
          handle(o);
      });
    });
  }
}

export class FromPromise extends Process {
  constructor(promise) {
    super();
    this._promise = Promise.resolve(promise);
  }

  map(fn) {
    const p = new FromPromise(this._promise.then(a => fn(a)));
    p._cancellationToken = this._cancellationToken;
    return p;
  }

  run(handle) {
    return this._promise;
  }
}

export class FromIterator extends Process {
  constructor(iterator, cancellationToken = undefined) {
    super(cancellationToken);
    this._iterator = iterator;
    this._promise = new Promise((resolve, reject) => {
      this._resolve = resolve;
      this._reject = reject;
    });
  }

  map(fn) {
    const iterator = function*() {
      const a = yield* this._iterator;
      return fn(a);
    };
    const p = new FromIterator(iterator, this._cancellationToken);
    p._cancellationToken = this._cancellationToken;
    return p;
  }

  run(handle) {
    this._handle = handle;
    this._onResolve();
    return this._promise;
  }

  _onResolve = input => {
    if (this.isCancelled) return;

    try {
      const step = this._iterator.next(input);
      Promise.resolve(step).then(this._onNext);
    } catch (err) {
      return this._reject(err);
    }
  };

  _onReject = err => {
    try {
      if (isFunction(this._iterator.throw)) {
        this._onNext(this._iterator.throw(err));
      } else {
        throw err;
      }
    } catch (err) {
      return this._reject(err);
    }
  };

  _onNext = ({ value, done }) => {
    if (this.isCancelled) return;

    if (done)
      return this._resolve(value);

    Process
      .of(value, this._cancellationToken, a => new Yield(a))
      .run(o => {
        if (this.isCancelled) return;
        this._handle(o)
      })
      .then(this._onResolve, this._onReject);
  };
}

class Parallel extends Process {
  constructor(processes, combineOutputs = identity, combinePromises = Promise.resolve) {
    super();
    this._outputs = Array(processes.length).fill(null);
    this._combineOutputs = combineOutputs;
    this._combinePromises = combinePromises;
    this._processes = processes.map(Process.of);
  }

  cancel() {
    super.cancel();
    this._processes.forEach(p => p.cancel());
  }

  run(handle) {
    const promises = this._processes.map((p, i) => p.run(o => {
      if (this.isCancelled) return;
      this._outputs[i] = o;
      handle(this._combineOutputs(this._outputs));
    }));

    return this._combinePromises(promises).catch(err => {
      this._processes.forEach(p => p.cancel())
      return Promise.reject(err);
    });
  }
}

export class Forever extends Parallel {
  constructor(processes, combineOutputs = identity) {
    super(processes, combineOutputs, _ => new Promise(_ => {}));
  }

  map(fn) {
    const p = new Forever(this._processes, this._combineOutputs);
    p._cancellationToken = this._cancellationToken;
    return p;
  }
}

export class Race extends Parallel {
  constructor(processes, combine = identity) {
    super(processes, combine, promises => {
      return Promise
        .race(promises)
        .then(a => {
          // Cancel losing processes
          this._processes.forEach(p => p.cancel());
          return a;
        });
    });
  }

  map(fn) {
    const p = new Race(this._processes, this._combineOutputs);
    p._cancellationToken = this._cancellationToken;
    const combinePromises = p._combinePromises;
    p._combinePromises = promises => combinePromises(promises).then(a => fn(a));
    return p;
  }
}

export class All extends Parallel {
  constructor(processes, combine = identity) {
    super(processes, combine, promises => Promise.all(promises));
  }

  map(fn) {
    const p = new Race(this._processes, this._combineOutputs);
    p._cancellationToken = this._cancellationToken;
    const combinePromises = p._combinePromises;
    p._combinePromises = promises => combinePromises(promises).then(as => as.map(a => fn(a)));
    return p;
  }
}
