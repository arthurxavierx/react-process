import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import VendingMachine from './VendingMachine';

// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

ReactDOM.render(
  <React.StrictMode>
    <VendingMachine />
  </React.StrictMode>,
  document.getElementById('root')
);
