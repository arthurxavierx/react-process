/** @jsx jsx */

import { delay, loop } from './process/combinators';
import componentFromProcess, { All, Forever, jsx, Race, render } from './process/react';

const Button = ({ value, onClick, children }) =>
  render(done =>
    <button
      onClick={_ => {
        if (typeof onClick === 'function')
          onClick();
        done(value);
      }}
    >
      {children}
    </button>
  );

function* Counter({ count = 0 } = {}) {
  while (true) {
    yield <Button>{count}</Button>;
    count = count + 1;
  }
};

function* Button1({ children }) {
  yield <Button>{children}</Button>;
  return 1;
}

function* Test() {
  yield <Button>Click me!</Button>;
  yield <p>Now wait...</p>;
  yield delay(1000);
  yield <p>Finish!</p>;
}

function* Clock(count = 0) {
  while (true) {
    yield <p>{count}</p>;
    yield delay(1000);
    count = count + 1;
  }
}

function* ButtonAndWindow() {
  yield <Button>Open</Button>;
  yield (
    <Race>
      <Forever><Button>Open</Button></Forever>
      <Button>Close window</Button>
    </Race>
  );
};

function* FetchData() {
  const Loading = _ => loop(async function*() {
    yield <p>Loading.</p>;
    await delay(200);
    yield <p>Loading..</p>;
    await delay(200);
    yield <p>Loading...</p>;
    await delay(200);
  });
  const fakeFetch = async function() {
    await delay(1000);
    // Uncomment the line below to see how exceptions are handled.
    // throw 'Random error';
    return 'Hello, world!';
  };

  yield <Button>Click to load some fake remote data</Button>;
  try {
    const data = yield (
      <Race>
        <Loading />
        {fakeFetch()}
      </Race>
    );
    yield <p>Success: {data}</p>;
  }
  catch(err) {
    yield <p>Failed: {err}</p>;
  }
}

// const App = componentFromProcess(Counter);
// const App = componentFromProcess(Test);
// const App = componentFromProcess(Clock);
// const App = componentFromProcess(ButtonAndWindow);
// const App = componentFromProcess(loop(ButtonAndWindow));
const App = componentFromProcess(FetchData);

export default App;
