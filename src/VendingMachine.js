/** @jsx jsx */

import { delay, loop } from './process/combinators';
import componentFromProcess, { All, Forever, jsx, Race, render } from './process/react';

const Column = ({ children }) =>
  <div style={{ display: "flex", flexFlow: "column" }}>{children}</div>;

const Button = ({ value, onClick, children }) =>
  render(done =>
    <button
      onClick={_ => {
        if (typeof onClick === 'function')
          onClick();
        done(value);
      }}
    >
      {children}
    </button>
  );

const Input = ({ value }) =>
  render(done => <input type="text" value={value} onChange={e => done(e.target.value)} />);

function* Pay({ min = 0.0 }) {
  let amount = min;

  yield loop(function*(done) {
    amount = yield (
      <Column>
        {<Input value={amount} />}
        <Forever>{!amount || amount < min ? `Please insert at least $${min}` : null}</Forever>
        <Button onClick={_ => amount >= min && done()} value={amount || 0.0}>Done</Button>
      </Column>
    );
  });

  return amount;
}

async function* Change({ change }) {
  yield <p>Here's your change: ${change}</p>;
  await delay(3000);
}

async function* ReturnCash({ amount }) {
  yield <p>Here's your cash back: ${amount}</p>;
  await delay(3000);
}

async function* ServeSoda() {
  yield <p>Here's your soda: 🍾</p>;
  await delay(3000);
}

async function* ServeTea() {
  yield <p>Here's your tea: ☕️</p>;
  await delay(3000);
}

async function* VendingMachine({ price }) {
  const amount = yield <Pay min={price} />;

  const change = amount - price;
  if (change > 0)
    yield <Change change={change} />;

  const option = yield (
    <Column>
      <Button value="cancel">Cancel</Button>
      <Button value="soda">Soda</Button>
      <Button value="tea">Tea</Button>
    </Column>
  );

  switch (option) {
    case "cancel":
      return yield <ReturnCash amount={price} />;
    case "soda":
      return yield <ServeSoda />;
    case "tea":
      return yield <ServeTea />;
  }
}

export default componentFromProcess(function*() {
  while (true) {
    yield <VendingMachine price={4.0} />;
  }
});
